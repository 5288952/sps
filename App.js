import React, {useState} from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Button } from 'react-native-paper';

const HelloWorldApp = () => {
  const [count, setCount] = useState(0);

  /*return (
    <View style={styles.container}>
      <Text style={styles.nadpis}>Štúdium</Text>
      <View style={styles.buttony}>
        <Button style={styles.button} title='Harmonogram akademického roka'/>
        <Button style={styles.button} title='Ubytovanie a stravovanie'/>
        <Button style={styles.button} title='Informácie pre študentov so špeciálnymi potrebami'/>
        <Button style={styles.button} title='Dokumenty a študijná legislatíva'/>
        <Button style={styles.button} title='Kontakty'/>
      </View>
    </View>
  );*/
  return (
    <Button onPress={() => {}}>Tlačidlo</Button>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 16
  },
  nadpis: {
    fontWeight: "bold",
    fontSize: 24,
    color: "orange"
  },
  button: {
    padding: 24
  }
});

export default HelloWorldApp;