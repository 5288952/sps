import { Avatar, List, Button, Title, Paragraph, Card, Provider as PaperProvider, Divider } from "react-native-paper"
import { FlatList, ScrollView, StyleSheet, View } from 'react-native';
import * as React from "react";
import { ROUTE_PROGRAM } from "../index";


const Predmet = ({ item }) => {
    //console.log(predmet);
    return (
        <Card style={styly.m8}>
            <Card.Content>
                <Title>{item.title}</Title>
                <Paragraph>UPJŠ ponúka 257 študijných programov</Paragraph>
            </Card.Content>
        </Card>
    )
}

const Fakulty = (props) => {
    //const [expanded, setExpanded] = React.useState(true);
    //const kliknut = () => setExpanded(!expanded);
    const odborOnPress = (odbor) => {
        props.navigation.navigate("Program", { odbor: odbor, appBarNadpis: odbor.nazov });
    }
    return (
        <List.Section title="Programy podľa fakulty">
            <List.AccordionGroup>
            {
                props.fakulty.map((fakulta, idx) => (
                    <List.Accordion
                        title={fakulta.nazov}
                        id={idx+1}>
                        <List.Section title="Bakalárske">
                            {
                                fakulta.odbory.bakalarske.map(odbor => (
                                    <List.Item title={odbor.nazov} description={odbor.skratka} onPress={() => odborOnPress(odbor)}></List.Item>
                                ))
                            }
                        </List.Section>
                        <Divider/>
                        <List.Section title="Magisterské">
                            {
                                fakulta.odbory.magisterske.map(odbor => (
                                    <List.Item title={odbor.nazov} description={odbor.skratka} onPress={() => odborOnPress(odbor)}></List.Item>
                                ))
                            }
                        </List.Section>
                        <Divider/>
                        <List.Section title="Doktorandské">
                            {
                                fakulta.odbory.doktorandske.map(odbor => (
                                    <List.Item title={odbor.nazov} description={odbor.skratka} onPress={() => odborOnPress(odbor)}></List.Item>
                                ))
                            }
                        </List.Section>
                    </List.Accordion>
                ))
            }
            </List.AccordionGroup>
        </List.Section>
    );
}

const ProgramyAktivita = ({ navigation, back }) => {
    /*var predmety = [
        { 
            id: 1,
            title: "Základy Linuxu" 
        },
        { 
            id: 2,
            title: "Databázy" 
        }
    ]*/
    var fakulty = [
        {
            nazov: "Prírodovedecká fakulta",
            odbory: {
                bakalarske: [
                    { 
                        nazov: "Informatika",
                        skratka: "Ib",
                        forma: "denná",
                        typ: "jednoodborové štúdium",
                        jazyky: "SK"
                    },
                    { 
                        nazov: "Matematika",
                        skratka: "Mb",
                        forma: "denná",
                        typ: "jednoodborové štúdium",
                        jazyky: "SK"
                    }
                ],
                magisterske: [
                    { 
                        nazov: "Informatika",
                        skratka: "Im",
                        forma: "denná",
                        typ: "jednoodborové štúdium",
                        jazyky: "SK"
                    },
                    { 
                        nazov: "Matematika",
                        skratka: "Mm",
                        forma: "denná",
                        typ: "jednoodborové štúdium",
                        jazyky: "SK"
                    }
                ],
                doktorandske: [
                    { 
                        nazov: "Informatika",
                        skratka: "Id",
                        forma: "denná",
                        typ: "jednoodborové štúdium",
                        jazyky: "SK"
                    },
                    { 
                        nazov: "Matematika",
                        skratka: "Md",
                        forma: "denná",
                        typ: "jednoodborové štúdium",
                        jazyky: "SK"
                    }
                ]
            }
        },
        {
            nazov: "Právnická fakulta",
            odbory: {
                bakalarske: [
                    { 
                        nazov: "Právo",
                        skratka: "PB3d",
                        forma: "denná",
                        typ: "jednoodborové štúdium",
                        jazyky: "SK"
                    },
                    { 
                        nazov: "Právo",
                        skratka: "PLB4d",
                        forma: "denná",
                        typ: "jednoodborové štúdium",
                        jazyky: "SK, FR, EN"
                    }
                ],
                magisterske: [
                    { 
                        nazov: "Právo",
                        skratka: "PM2d",
                        forma: "denná",
                        typ: "jednoodborové štúdium",
                        jazyky: "SK"
                    }
                ],
                doktorandske: [
                    { 
                        nazov: "Medzinárodné právo",
                        skratka: "MPd",
                        forma: "denná",
                        typ: "jednoodborové štúdium",
                        jazyky: "SK"
                    },
                    { 
                        nazov: "Obchodné právo",
                        skratka: "OFPd",
                        forma: "denná",
                        typ: "jednoodborové štúdium",
                        jazyky: "SK"
                    },
                    { 
                        nazov: "Trestné právo",
                        skratka: "TPd",
                        forma: "denná",
                        typ: "jednoodborové štúdium",
                        jazyky: "SK"
                    },
                    { 
                        nazov: "Občianske právo",
                        skratka: "OPd",
                        forma: "denná",
                        typ: "jednoodborové štúdium",
                        jazyky: "SK"
                    }
                ]
            }
        },
    ]

    var predmety = [...Array(10).keys()].map((i) => {
        return { id: i, title: "Databazy" }
    });
    return (
        <PaperProvider>
            <ScrollView>
            <Fakulty navigation={navigation} fakulty={fakulty}></Fakulty>
            </ScrollView>
        </PaperProvider>
    )
}

const styly = StyleSheet.create({
    m8: {
        margin: 8
    },
    pt8: {
        paddingTop: 8
    }
});

export default ProgramyAktivita;