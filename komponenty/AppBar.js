import * as React from 'react';
import { Appbar } from 'react-native-paper';

const AppBar = ({navigation, back, route}) => {
    console.log("AB");
    console.log(navigation);
    console.log(back);
    console.log(route);
    return (
        <Appbar.Header>
            { back ? <Appbar.BackAction onPress={navigation.goBack}/> : null }
            <Appbar.Content title={(route.params && route.params.appBarNadpis) ? route.params.appBarNadpis : route.name} />
            <Appbar.Action icon="magnify" onPress={() => { }} />
        </Appbar.Header>
        )
    }

export default AppBar;