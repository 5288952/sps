import {WebView} from "react-native-webview"

const WebAktivita = ({navigation, route}) => {
    return (
        <WebView
            source={{ uri: route.params.url}}
        >

        </WebView>
    )
}

export default WebAktivita;