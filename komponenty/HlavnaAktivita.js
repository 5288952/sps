import { StyleSheet } from 'react-native';
import { Avatar, List, Button, Title, Paragraph, Card, Provider as PaperProvider, Divider } from "react-native-paper"
import AppBar from './AppBar';
import {ROUTE_PROGRAMY} from '../index.js'


const HlavnaAktivita = ({ navigation }) => {
    return (
        <PaperProvider>
            <Card style={styly.paper}>
                <Card.Content>
                    <Title>Študijné programy na jednom mieste</Title>
                    <Paragraph>UPJŠ ponúka 257 študijných programov</Paragraph>
                </Card.Content>
                <Card.Actions>
                    <Button icon="arrow-right" onPress={() => {navigation.navigate(ROUTE_PROGRAMY, {})}}>Zobraziť</Button>
                </Card.Actions>
            </Card>
            <List.Section>
                <List.Subheader>Odkazy</List.Subheader>
                <List.Item
                    title="Harmonogram akademického roka"
                    onPress={() => {}}
                />
                <List.Item
                    title="Ubytovanie a stravovanie"
                    onPress={() => {}}
                />
                <List.Item
                    title="Informácie pre študentov so špeciálnymi potrebami"
                    onPress={() => {}}
                />
                <List.Item
                    title="Dokumenty a študijná legislatíva"
                    onPress={() => {}}
                />
                <List.Item
                    title="Kontakty"
                    onPress={() => {}}
                />
            </List.Section>
        </PaperProvider>
    )
}

const styly = StyleSheet.create({
    paper: {
        margin: 8
    }
});

export default HlavnaAktivita;