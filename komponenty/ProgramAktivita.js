import { Avatar, List, Button, Title, Paragraph, Card, Text, DataTable, Provider as PaperProvider, Chip } from "react-native-paper"
import { FlatList, SectionList, StyleSheet, View } from 'react-native';
import * as React from "react";
import { ROUTE_WEB } from "..";


const Predmet = ({item, onPress}) => {
    //console.log(predmet);
    return (
        <Card style={styly.m8} onPress={onPress}>
            <Card.Content>
                <Title>{item.title}</Title>
                <Paragraph>{"ÚINF/ZLI/21"}</Paragraph>
                <View style={styly.chips}>
                    <Chip style={styly.chip}>8 kreditov</Chip>
                    <Chip style={styly.chip}>1C+2P</Chip>
                    <Chip style={styly.chip}>1 Z</Chip>
                </View>
            </Card.Content>
        </Card>
    )
}

const Tabulka = ({odbor}) => {
    return (
        <Card style={styly.m8}>
            <Card.Title title="Detaily programu"></Card.Title>
            <DataTable>
                <DataTable.Row>
                    <DataTable.Cell>Skratka</DataTable.Cell>
                    <DataTable.Cell>{odbor.skratka}</DataTable.Cell>
                </DataTable.Row>
                <DataTable.Row>
                    <DataTable.Cell>Forma</DataTable.Cell>
                    <DataTable.Cell>{odbor.forma}</DataTable.Cell>
                </DataTable.Row>
                <DataTable.Row>
                    <DataTable.Cell>Typ štúdia</DataTable.Cell>
                    <DataTable.Cell>{odbor.typ}</DataTable.Cell>
                </DataTable.Row>
                <DataTable.Row>
                    <DataTable.Cell>Jazyky</DataTable.Cell>
                    <DataTable.Cell>{odbor.jazyky}</DataTable.Cell>
                </DataTable.Row>
            </DataTable>
        </Card>
    )
}

const HlavickaSekcie = ({title}) => {
    return (
        <Text variant="headlineSmall" style={styly.plt8}>{title}</Text>
    )
}

const ProgramAktivita = ({navigation, route}) => {
    //console.log("PROGRAM");
    //console.log(props)
    var predmetySekcie = [
        {
            title: "Povinné predmety",
            data: [
                { 
                    id: 1,
                    title: "Základy Linuxu" 
                },
                { 
                    id: 2,
                    title: "Databázy" 
                }
            ]
        },
        {
            title: "Povinne voliteľné predmety",
            data: [
                { 
                    id: 1,
                    title: "Základy Linuxu" 
                },
                { 
                    id: 2,
                    title: "Databázy" 
                }
            ]
        },

        {
            title: "Výberové predmety",
            data: [
                { 
                    id: 1,
                    title: "Základy Linuxu" 
                },
                { 
                    id: 2,
                    title: "Databázy" 
                }
            ]
        },
    ]
    

    var predmety =  [...Array(10).keys()].map((i) => {
        return { id: i, title: "Databazy"}
    });
    return (
        <PaperProvider>
            <SectionList
                sections={predmetySekcie}
                renderItem={({item}) => <Predmet item={item} onPress={() => {navigation.navigate(ROUTE_WEB,{
                    url:"https://studijne-programy.upjs.sk/predmet/%C3%9AINF%2FZLI%2F21",
                    appBarNadpis: item.title
                })}}/>}
                renderSectionHeader={({ section: {title}}) => (
                    <HlavickaSekcie title={title}/>
                )}
                ListHeaderComponent={() => <Tabulka odbor={route.params.odbor}/>}
                keyExtractor={(predmet) => predmet.id}
            >

            </SectionList>
        </PaperProvider>
    )
}

const styly = StyleSheet.create({
    m8: {
        margin: 8
    },
    pt8: {
        paddingTop: 8
    },
    plt8: {
        paddingLeft: 8,
        paddingTop: 8
    },
    chip: {
        marginRight: 8
    },
    chips: {
        marginTop: 8,
        flexDirection: "row"
    }
});

export default ProgramAktivita;