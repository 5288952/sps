/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import { StyleSheet } from 'react-native';
import { Avatar, List, Button, Title, Paragraph, Card, Provider as PaperProvider, Divider } from "react-native-paper"
import AppBar from './komponenty/AppBar';
import HlavnaAktivita from "./komponenty/HlavnaAktivita"
import ProgramyAktivita from './komponenty/ProgramyAktivita';
import ProgramAktivita from './komponenty/ProgramAktivita';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import WebAktivita from './komponenty/WebAktivita';

const LeftContent = props => <Avatar.Icon {...props} icon="folder" />

const Stack = createNativeStackNavigator();

export const ROUTE_DOMOV = "Domov";
export const ROUTE_PROGRAMY = "Študijné programy";
export const ROUTE_PROGRAM = "Program";
export const ROUTE_WEB = "Web";

export default function Main() {
    return (
        <NavigationContainer>
            <Stack.Navigator 
                initialRouteName={ROUTE_DOMOV}
                screenOptions={{
                    header: (props) => <AppBar {...props} />
                }}>
                <Stack.Screen name={ROUTE_DOMOV} component={HlavnaAktivita}/>
                <Stack.Screen 
                    name={ROUTE_PROGRAMY}
                    component={ProgramyAktivita}
                />
                <Stack.Screen 
                    name={ROUTE_PROGRAM}
                    component={ProgramAktivita}
                />
                <Stack.Screen
                    name={ROUTE_WEB}
                    component={WebAktivita}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
}


const styly = StyleSheet.create({
    paper: {
        margin: 8
    }
});


AppRegistry.registerComponent(appName, () => Main);
